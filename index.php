<?php
require_once "controladores/plantilla.controlador.php";
require_once "controladores/usuario.controlador.php";
require_once "controladores/categoria.controlador.php";
require_once "controladores/producto.controlador.php";
require_once "controladores/cliente.controlador.php";
require_once "controladores/venta.controlador.php";

require_once "modelos/MUsuario.php";
require_once "modelos/MCategoria.php";
require_once "modelos/MProducto.php";
require_once "modelos/Mcliente.php";
require_once "modelos/MVenta.php";

$plantilla= new ControladorPlantilla();
$plantilla -> ctrPlantilla();